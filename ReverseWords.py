import bisect
import inspect
from datetime import datetime
from typing import List


def timed(func):
    def decorator(*args, **kwargs):
        start = datetime.timestamp(datetime.now())
        result = func(*args, **kwargs)
        print(f'duration: {datetime.timestamp(datetime.now()) - start}')
        return result

    return decorator


def logged(func):
    """
    This is just an exercise in decorators
    Remeber that logged uses function name for, well, logging
    If @logger is not used as the closest to real function annotation,
    it will print a name of inner decorator function from the decorator
    that follows this one in case this one is not the last on the list
    :param func: any function
    :return: decorated function
    """
    members = inspect.getmembers(func)
    n = bisect.bisect_left(members, ('__name__',))
    name = members[n][1]

    def decorator(*args, **kwargs):
        print(f'[{datetime.now()}] - [{name}]')
        return func(*args, **kwargs)

    return decorator


@timed
@logged
def reverseWords(sentence: str) -> str:
    return ' '.join([w[::-1] for w in sentence.strip().split(' ')])


testStrings: List[str] = [
    'I am Oleg Tsvinev',
    '     I am Oleg Tsvinev'
]

for s in testStrings:
    print(f"\"{reverseWords(s)}\"")
